# projet-test-xp

## Contexte

L'objectif est de réaliser un agenda partagé qui permettra à une collectivité, ou une association de permettra à ses
membres/adhérents/communauté de rajouter des événements sur un calendrier qui pourra être consulter par n'importe qui.


## Contrainte de travail

L'idée va être de tenter d'appliquer les principes de l'extreme programming. 

On va donc partir sur une gestion de planning et une conception Agile.

* Définir toutes les user stories ("En tant que ... je veux pouvoir ... [afin de ...]) de l'application
    * Pour chaque user stories, définir ensembles les tests d'acceptances
    * Faire les maquettes fonctionnelles des user stories principales
* Faire un planning poker afin de déterminer la "difficulté" de chaque user stories puis les ordonancer par importance (utiliser gitlab et ses issues pour mettre les résultats par écrit)
* Pour la partie code, l'idée sera de faire ça en TDD et en Ping pong pair programming 